import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
//import { map } from 'rxjs/operators';
import { EmployeeListResponse } from '../models/EmployeeListResponse';
import { CatalogResponse } from '../models/CatalogResponse';
import { EmployeeRequest } from '../models/EmployeeRequest';
import { SalaryRequest } from '../models/SalaryRequest';
import { errorResponse } from '../models/errorResponse';
import { apiResponse } from '../apiResponse/apiResponse';


const apiUrl = 'https://localhost:5001/api/'

@Injectable({
  providedIn: 'root'
})


export class RestService {

  errorResp: errorResponse;
  response: apiResponse;

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
      //this.errorResp = new errorResponse(error.error.message,error.statusText,error.type.toString());
      this.response =  new apiResponse(error.error.message,error.statusText,error.type.toString());
    } else {
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
      this.response = new apiResponse(error.error.Message, error.error.StatusCode, error.error.ExceptionType)
        error.error;
    }
    //return of(error.error);
    /*throwError('Something bad happened; please try again later.');*/
    return of(this.response);
  }

  private extractData(res: Response): any {
    const body = res;
    return body || {};
  }

  getEmployees(office: string = '', grade: string = '', position: string = '', pageIndex:number=1,pageSize:number=8): Observable<apiResponse> {
    let filterParam: string = '';
    if (grade != '')
      filterParam = '?Grade=' + grade;
    if (position != '')
      filterParam = filterParam + '&PositionId=' + position;
    if (office != '')
      filterParam = filterParam + "&OfficeId=" + office;

    if (filterParam == '') filterParam= '?pageIndex=' + pageIndex + '&pageSize=' + pageSize;
    else filterParam += '&pageIndex=' + pageIndex + '&pageSize=' + pageSize;

    return this.http.get<apiResponse>(apiUrl + 'Employee/GetList' + filterParam).pipe(
      //map(this.extractData),
      catchError(this.handleError)
    );
  }

  getEmployee(code: string): Observable<apiResponse> {
    return this.http.get<apiResponse>(apiUrl + 'Employee/GetById/' + code).pipe(
      //map(this.extractData),
      catchError(this.handleError)
    );
  }

  getCatalog(option: string): Observable<apiResponse> {
    return this.http.get<apiResponse>(apiUrl + 'Catalog/get' + option).pipe(
      catchError(this.handleError)
    );
  }

  addUpdateEmployee(employee: EmployeeRequest): Observable<apiResponse> {
    if (employee.id != null) {
      return this.http.put<apiResponse>(apiUrl + 'Employee/' + employee.id, employee).pipe(
        catchError(this.handleError));     
    }
    else {
      return this.http.post<apiResponse>(apiUrl + 'Employee', employee).pipe(
        catchError(this.handleError)
      );
    }
  }

  addUpdateEmployeeSalary(salary: SalaryRequest):Observable<apiResponse> {
    if (salary.id != null) {
      return this.http.post<apiResponse>(apiUrl + 'Salary/UpdateSalary/' + salary.id, salary).pipe(
        catchError(this.handleError));
    }
    else {
      return this.http.post<apiResponse>(apiUrl + 'Salary', salary).pipe(
        catchError(this.handleError));
    }

  }

  searchEmployeeBonus(code: string): Observable<apiResponse>{
    return this.http.get<apiResponse>(apiUrl + 'Employee/search?code=' + code).pipe(
      catchError(this.handleError)
    );
  }



  

}
