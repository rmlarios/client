"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiResponse = void 0;
var apiResponse = /** @class */ (function () {
    function apiResponse(m, s, t) {
        this.isError = true;
        this.exceptionType = null;
        this.message = m;
        this.statusCode = s;
        this.isError = true;
        this.exceptionType = t;
    }
    return apiResponse;
}());
exports.apiResponse = apiResponse;
//# sourceMappingURL=apiResponse.js.map