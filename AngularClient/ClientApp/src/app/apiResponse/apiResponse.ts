export class apiResponse {
  readonly message?: string;
  readonly isError?: boolean = true;
  readonly statusCode?: string;
  readonly exceptionType?: string = null;
  readonly data?: any;
  readonly datas?: any[];
  readonly count?: number;

  

  constructor(m: string, s: string, t: string) {
    this.message = m;
    this.statusCode = s;
    this.isError = true;
    this.exceptionType = t;
    }

/*  public string Message { get; set; } = "";
    public bool IsError { get; set; } = false;
    public string StatusCode { get; set; } = HttpStatusCode.OK.ToString();
    public string ExceptionType { get; set; } = null;
    public T Data { get; set; }
    public List < T > Datas { get; set; }
    public int Count { get; set; } = 0;*/
}
