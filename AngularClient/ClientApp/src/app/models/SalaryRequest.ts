import { Guid } from "guid-typescript";

export interface SalaryRequest {
  id: Guid;
  year: number;
  month: number;
  baseSalary: number;
  productionBonus: number;
  compesationBonus: number;
  comission: number;
  contributions: number; 
  employeeId: Guid;
  startDate: Date;
}
