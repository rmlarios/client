import { Guid } from "guid-typescript";

export interface EmployeeRequest {
  
  employeeName: string;
  employeeSurname: string;
  employeeCode: string;
  grade: number;
  birthDay: Date;
  officeId: Guid;
  divisionId: Guid;
  positionId: Guid;  
  identificationNumber: string;
  id: Guid;
  
  
}
