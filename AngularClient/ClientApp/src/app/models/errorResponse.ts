export class errorResponse {
 readonly Message: string;
 readonly StatusCode: string;
 readonly Type: string;
 readonly IsError: boolean = true;

  constructor(m:string,s:string,t:string) {
    this.Message = m;
    this.StatusCode = s;
    this.Type = t;
    
  }

}
