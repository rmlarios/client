import { Guid } from "guid-typescript";
import { SalaryReponse } from "./SalaryResponse";

export interface EmployeeListResponse {
  id: Guid;
  employeeName: string;
  employeeSurname: string;
  employeeCode: string;
  identificationNumber: string;
  grade: number;
  birthDay: Date;
  officeId: Guid;
  divisionId: Guid;
  positionId: Guid;
  totalSalary: number;
  officeName?: string;
  divisionName?: string;
  positionName?: string;

  salaries: Array<SalaryReponse>;
  bonus?: number;
}

/*
        public Guid Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public string EmployeeCode { get; set; }
        public string IdentificationNumber { get; set; }
        public int Grade { get; set; }
        public DateTime BirthDay { get; set; }
        public Guid OfficeId { get; set; }
        public Guid DivisionId { get; set; }
        public Guid PositionId { get; set; }
        public Double TotalSalary { get; set; } = 0;
  
 */
