"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorResponse = void 0;
var errorResponse = /** @class */ (function () {
    function errorResponse(m, s, t) {
        this.IsError = true;
        this.Message = m;
        this.StatusCode = s;
        this.Type = t;
    }
    return errorResponse;
}());
exports.errorResponse = errorResponse;
//# sourceMappingURL=errorResponse.js.map