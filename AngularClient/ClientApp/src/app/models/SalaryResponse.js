"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalaryReponse = void 0;
var SalaryReponse = /** @class */ (function () {
    function SalaryReponse() {
        this.baseSalary = 0;
        this.productionBonus = 0;
        this.compesationBonus = 0;
        this.comission = 0;
        this.contributions = 0;
        this.otherIncome = (this.baseSalary + this.comission) * 0.08 + this.comission;
        this.totalSalary = this.baseSalary + this.productionBonus + (this.compesationBonus * 0.75) + this.otherIncome - this.contributions;
        this.modified = false;
        //Base Salary + Production Bonus + (Compensation Bonus * 75%) + Other Income - Contributions
    }
    return SalaryReponse;
}());
exports.SalaryReponse = SalaryReponse;
//# sourceMappingURL=SalaryResponse.js.map