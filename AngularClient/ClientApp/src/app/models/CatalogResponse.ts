import { Guid } from "guid-typescript";

export interface CatalogResponse {
  name: string;
  id: Guid;
}
