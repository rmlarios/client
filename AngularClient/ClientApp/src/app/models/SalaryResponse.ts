import { Guid } from "guid-typescript";

export class SalaryReponse {
  id: Guid;
  year: number;
  month: number;
  baseSalary: number = 0;
  productionBonus: number = 0;
  compesationBonus: number =0;
  comission: number = 0;
  contributions: number = 0;
  otherIncome?: number = (this.baseSalary + this.comission) * 0.08 + this.comission;
  totalSalary?: number = this.baseSalary + this.productionBonus + (this.compesationBonus*0.75) + this.otherIncome - this.contributions;
  employeeId: Guid;
  startDate: Date;

  modified: boolean = false;

  //Base Salary + Production Bonus + (Compensation Bonus * 75%) + Other Income - Contributions
}
