import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryEditformComponent } from './salary-editform.component';

describe('SalaryEditformComponent', () => {
  let component: SalaryEditformComponent;
  let fixture: ComponentFixture<SalaryEditformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaryEditformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryEditformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
