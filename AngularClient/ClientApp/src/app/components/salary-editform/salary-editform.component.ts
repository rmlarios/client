import { DatePipe, formatCurrency, formatNumber } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SalaryReponse } from '../../models/SalaryResponse';
import { RestService } from '../../services/rest.service';
import { EmployeeEditComponent } from '../employee-edit/employee-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-salary-editform',
  templateUrl: './salary-editform.component.html',
  styleUrls: ['./salary-editform.component.css']
})
export class SalaryEditformComponent implements OnInit {
  public salaryForm: FormGroup;
  formTitle: string;
  hasChange: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SalaryReponse,
    private dialogRef: MatDialogRef<SalaryEditformComponent>,
    private rest: RestService,
    //private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.onCreateGroupFormValueChange();
  }

  initializeForm() {
    this.data.modified = false;
    this.salaryForm = this.formBuilder.group({
      id: [this.data && this.data.id],
      baseSalary: [this.data && this.data.baseSalary, [Validators.required,Validators.min(0)]],
      //year: [this.data && this.data.year, [Validators.required,Validators.min(1900)]],
      //month: [this.data && this.data.month, Validators.required],
      yearmonth: [this.data && this.data.id && this.data.year.toString() + '-' + formatNumber(this.data.month,'en-US','2.0').toString()],
      productionBonus: [this.data && this.data.productionBonus, [Validators.required,Validators.min(0)]],
      compesationBonus: [this.data && this.data.compesationBonus, [Validators.required,Validators.min(0)]],
      comission: [this.data && this.data.comission, [Validators.required,Validators.min(0)]],
      contributions: [this.data && this.data.contributions, [Validators.required, Validators.min(0)]],
      startDate: [this.data && new DatePipe('en').transform(this.data.startDate, 'yyyy-MM-dd'),Validators.required]
    }
    );    

    if (this.salaryForm.get('id').value === "" || this.salaryForm.get('id').value == null) {
      this.formTitle = "Register Salary";
    }
    else {
      this.formTitle = "Edit Salary";
    }
  }
  error: any = {
    isError: false,
    errorMessage: ''
  };

  compareTwoDates() {
    if (this.salaryForm.get('startDate').value == null || this.salaryForm.get('yearmonth').value)
      return;

    if (new Date(this.salaryForm.controls['yearmonth'].value) < new Date(this.salaryForm.controls['startDate'].value)) {
      this.error = {
        isError: true,
        errorMessage: 'Month payed must be less than Begin Date.'
        
      };
      this.matSnackBar.open('Month payed must be less than Begin Date', 'Close', {duration:3000});

      this.salaryForm.get('yearmonth').reset();
         }
    }


  onCreateGroupFormValueChange() {
    const initialValue = this.salaryForm.value
    this.salaryForm.valueChanges.subscribe(value => {
      this.hasChange = Object.keys(initialValue).some(key => this.salaryForm.value[key] !=
        initialValue[key])
    });
  }

  onsubmit() {
    if (this.salaryForm.valid ) {     
      let newValues: SalaryReponse = ({
        id: this.data.id,
        baseSalary: this.salaryForm.value.baseSalary,
        year : this.salaryForm.value.yearmonth.split('-')[0],
        month: parseInt(this.salaryForm.value.yearmonth.split('-')[1]),
        productionBonus : this.salaryForm.value.productionBonus,
        compesationBonus : this.salaryForm.value.compesationBonus,
        comission : this.salaryForm.value.comission,
        contributions : this.salaryForm.value.contributions,
        modified : this.hasChange,
        employeeId: this.data.employeeId,
        startDate: this.salaryForm.value.startDate,
        otherIncome: (this.salaryForm.value.baseSalary + this.salaryForm.value.comission) * 0.08 + this.salaryForm.value.comission
        //totalSalary: this.salaryForm.value.baseSalary + this.salaryForm.value.productionBonus + (this.salaryForm.value.compesationBonus * 0.75) +this.salaryForm.value.otherIncome - this.salaryForm.value.contributions

      });
      newValues.totalSalary = newValues.baseSalary + newValues.productionBonus + (newValues.compesationBonus * 0.75) + newValues.otherIncome - newValues.contributions;
      this.dialogRef.close({ data: newValues });
      }
      //Update Local
      /*if (this.salaryForm.value.id == null) {
        this.data.baseSalary = this.salaryForm.value.baseSalary;
        this.data.year = this.salaryForm.value.yearmonth.split('-')[0];
        this.data.month = this.salaryForm.value.yearmonth.split('-')[1];
        this.data.productionBonus = this.salaryForm.value.productionBonus;
        this.data.compesationBonus = this.salaryForm.value.compesationBonus;
        this.data.comission = this.salaryForm.value.comission;
        this.data.contributions = this.salaryForm.value.contributions;
        this.data.modified = this.hasChange;
        //this.data.baseSalary = this.salaryForm.value.baseSalary;
        //this.data.baseSalary = this.salaryForm.value.baseSalary;
        this.dialogRef.close({ data: this.data });
      }*/     
      
      //this.dialogRef.closeAll();
      
    }
  }

