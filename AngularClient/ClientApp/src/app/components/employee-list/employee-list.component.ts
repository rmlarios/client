import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
//import { filter } from 'rxjs/src/operators';
import { EmployeeListResponse } from '../../models/EmployeeListResponse';
import { RestService } from '../../services/rest.service';
//import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../../moduls/ng-material/snack-bar/snack-bar.component'
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { apiResponse } from '../../apiResponse/apiResponse';



@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit, AfterViewInit {
  public currentEmployee = null;
  employees: EmployeeListResponse[] = [];
  public currentFilter: string = '';
  totalRecords: number = 0;
  currentFilterApplied
  public displayedColumns = ['options','employeeCode','employeeName', 'grade', 'divisionName','positionName','birthDay','birthDay','identificationNumber','totalSalary'];
   filterParamOffice: string = '';
filterParamGrade: string = '';
filterParamPosition: string = '';
  dataSource = new MatTableDataSource<EmployeeListResponse>(this.employees);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


  //selectionModel = new SelectionModel(true);0

  constructor(
    public rest: RestService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) { }
    ngAfterViewInit(): void {
        //throw new Error('Method not implemented.');
    }

  ngOnInit(): void {
    this.getEmployees();
    this.dataSource.paginator = this.paginator;
  }

  public selectEmployee(event: any, item: any) {
    this.currentEmployee = item;
  }

  public filterList(filter): void {
   
    this.currentFilter = filter;
    this.currentFilterApplied = '';

    if (filter === 'filterGrade') {
      this.filterParamGrade = this.currentEmployee.grade.toString();
      this.currentFilterApplied = 'Grade= ' + this.filterParamGrade;
    }
    else if (filter === 'filterOfficeGrade') {
      this.filterParamGrade = this.currentEmployee.grade.toString();
      this.filterParamOffice = this.currentEmployee.officeId.toString();
      this.currentFilterApplied = 'Office= ' + this.currentEmployee.officeName + ' And Grade=' + this.filterParamGrade;
    }
    else if (filter === 'filterPositionGrade') {
      this.filterParamGrade = this.currentEmployee.grade.toString();
      this.filterParamPosition = this.currentEmployee.positionId.toString();
      this.currentFilterApplied = 'Position= ' + this.currentEmployee.positionName + ' And Grade= ' + this.filterParamGrade;
    }

    this.getEmployees();
    /*this.rest.getEmployees(filterParamOffice, filterParamGrade, filterParamPosition).subscribe((resp: any) => {
      this.employees = resp;
      console.log(this.employees);
    }
    )*/


  }

  getEmployees(pageIndex:number=1,pageSize:number=8): void {
    this.rest.getEmployees(this.filterParamOffice, this.filterParamGrade, this.filterParamPosition,pageIndex,pageSize).subscribe((resp: apiResponse) => {
      if (resp.isError != true) {
        this.employees = resp.datas;
        this.totalRecords = resp.count;
        this.dataSource = new MatTableDataSource<EmployeeListResponse>(this.employees);
       // this.paginator.length = this.totalRecords;
       // this.dataSource.paginator = this.paginator;
        console.log(this.employees);
      }
      else {
        this.matSnackBar.open(resp.message, "Close", {
          duration: 4000
        });
      }
    }
    )
  }

  public gotoEdit(id): void {
    const navigationEditForm: string[] = ['/employee-edit/'];
    if (id != '0') {
      navigationEditForm.push(id);
    }

    this.router.navigate(navigationEditForm);
  }

  public openSnack(): void {
    this.matSnackBar.open("Update successful", "Okay");
  }

  pageEvent() {
    var pageIndex = this.paginator.pageIndex + 1;
    this.getEmployees(pageIndex, this.paginator.pageSize);
    

  }

}
