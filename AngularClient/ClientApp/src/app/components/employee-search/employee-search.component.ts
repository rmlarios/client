import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeListResponse } from '../../models/EmployeeListResponse';
import { RestService } from '../../services/rest.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../../moduls/ng-material/snack-bar/snack-bar.component'
import { errorResponse } from '../../models/errorResponse';
import { apiResponse } from '../../apiResponse/apiResponse';

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.css']
})
export class EmployeeSearchComponent implements OnInit {

  public form: FormGroup;  
  public employeeData: EmployeeListResponse;
  public displayedColumns = ['year', 'month', 'totalSalary'];

  constructor(
    private router: Router,
    private rest: RestService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private matSnackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.employeeData = ({} as EmployeeListResponse);
    //this.employeeData.salaries = [];
    this.form = this.formBuilder.group({
      searhCode : [null,Validators.required]
    });
  }

  public search(): void {
    console.warn(this.form.value);
    if (this.form.value.searhCode == null) return;
    //Saving Employee Data
    this.rest.searchEmployeeBonus(this.form.value.searhCode).subscribe((resp: apiResponse) => {
      console.log(resp);
      if (resp.isError != true) {
        this.employeeData = resp.data;
        if (this.employeeData.salaries == null) {          
          this.matSnackBar.open('Not salaries records registred for this Employee','Close',{duration : 2000});
        }        
      }
      else {
        this.matSnackBar.open(resp.message, "Close", {
          duration: 4000          
        });
        this.employeeData.salaries = null;
        this.employeeData.bonus = 0;
      }

    });
  }     

}
