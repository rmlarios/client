import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from '../../services/rest.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';
import { CatalogResponse } from '../../models/CatalogResponse';
import { MatDialog } from '@angular/material/dialog';
import { SalaryEditformComponent } from '../salary-editform/salary-editform.component';
import { SalaryReponse } from '../../models/SalaryResponse';
import { EmployeeListResponse } from '../../models/EmployeeListResponse';
import { Guid } from 'guid-typescript';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../../moduls/ng-material/snack-bar/snack-bar.component'
import { errorResponse } from '../../models/errorResponse';
import { apiResponse } from '../../apiResponse/apiResponse';


@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {
  public formGroup: FormGroup;

  //public employeeData: any;
  public employeeData: EmployeeListResponse=null;
  offices: CatalogResponse[] = [];
  divisions: CatalogResponse[] = [];
  positions: CatalogResponse[] = [];
  formTitle: string;
  hasChange: boolean = false;
  hasSalariesChange: boolean = false;

  constructor(
    private router: Router,
    private rest: RestService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private matSnackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (this.route.snapshot.params.id === undefined) {
      const newEmployee : EmployeeListResponse = ({
        id: null,
        employeeName: null,
        employeeSurname: null,
        employeeCode: null,
        grade: null,
        identificationNumber: null,
        birthDay: null,
        officeId: null,
        divisionId: null,
        positionId: null,
        totalSalary: null,
        salaries: null
        
      });
      this.employeeData = newEmployee;
      this.buildForm();
      this.loadCatalogs();
      this.onCreateGroupFormValueChange();
    }
    else {
      this.rest.getEmployee(this.route.snapshot.params.id).subscribe((data: apiResponse) => {
        console.log(data);
        if (data.isError != true) {
          this.employeeData = data.data;          
        }
        else {
          this.matSnackBar.open(data.message, 'Close');
        }
        this.buildForm();
        this.loadCatalogs();
        this.onCreateGroupFormValueChange();
        
       
      });
    }
  }

  private loadCatalogs() {
    this.rest.getCatalog('Offices').subscribe((resp: apiResponse) => {
      if (resp.isError != true) {
        this.offices = resp.datas;
        console.log(this.offices);
      }
     
      //this.formGroup.get('officeName').setValue(this.employeeData.officeId);
    }
    );
    this.rest.getCatalog('Divisions').subscribe((resp: apiResponse) => {
      if (resp.isError != true) {
        this.divisions = resp.datas;
        console.log(this.divisions);}
      
      //this.formGroup.get('officeName').setValue(this.employeeData.officeId);
    }
    );
    this.rest.getCatalog('Positions').subscribe((resp: apiResponse) => {
      this.positions = resp.datas;
      console.log(this.positions);
      //this.formGroup.get('officeName').setValue(this.employeeData.officeId);
    }
    );

  }

  private buildForm() {

    this.formGroup = this.formBuilder.group({
      id: [this.employeeData && this.employeeData.id],
      employeeName: [this.employeeData && this.employeeData.employeeName, Validators.required],
      employeeSurname: [this.employeeData && this.employeeData.employeeSurname, Validators.required],
      employeeCode: [this.employeeData && this.employeeData.employeeCode, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      identificationNumber: [this.employeeData && this.employeeData.identificationNumber, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      grade: [this.employeeData && this.employeeData.grade, [Validators.required,Validators.min(0)]],
      birthday: [this.employeeData && formatDate(this.employeeData.birthDay, 'yyyy-MM-dd', 'en-US'), Validators.required],
      //officeId: this.employeeData.officeId,
      divisionId: [this.employeeData && this.employeeData.divisionId, Validators.required],
      positionId: [this.employeeData && this.employeeData.positionId, Validators.required],
      officeId: [this.employeeData && this.employeeData.officeId, Validators.required]
    });

    if (this.formGroup.get('id').value === "" || this.formGroup.get('id').value == null) {
      this.formTitle = "Register Employee";
    }
    else {
      this.formTitle = "Edit Employee";
    }
  }

  onCreateGroupFormValueChange() {
    const initialValue = this.formGroup.value
    this.formGroup.valueChanges.subscribe(value => {
      this.hasChange = Object.keys(initialValue).some(key => this.formGroup.value[key] !=
        initialValue[key])
    });
  }

  private openForm(salary?: SalaryReponse) {
    if (typeof (salary) === "undefined") {
      const newSalary: SalaryReponse = ({
        id: null,
        year: null,
        month: null,
        baseSalary: null,
        productionBonus: null,
        compesationBonus: null,
        comission: null,
        contributions: null,
        otherIncome: null,
        totalSalary: null,
        employeeId: null,
        modified: true,
        startDate:null
      });
      salary = newSalary;
    }

    const dialogRef = this.dialog.open(SalaryEditformComponent, {
      data: salary,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.validateSalary(result);
      }
    })
  }

  validateSalary(result?: any) {
    if (result.data.modified) {
      this.hasSalariesChange = true;
      //Validating if Salary with the Same Year and Month is showed in the local Salaries Detail
      if (this.employeeData.salaries.filter(e => e.year == result.data.year && e.month == result.data.month && e.id != result.data.id).length != 0) {
        result.data.year = null;
        result.data.month = null;
        this.openForm(result.data);
        this.matSnackBar.open('Salary fot this Month and Year already exists.','Close',{duration : 4000});
      }
      else {
        if (result.data.id == null) {
          this.addLocalSalary(result.data);
        }
        else {
          this.updateLocalSalary(result.data);
        }
      }

    }
  }

  addLocalSalary(salary: SalaryReponse) {
    this.employeeData.salaries.push(salary);
  }

  updateLocalSalary(salary: SalaryReponse) {
    let itemIndex = this.employeeData.salaries.findIndex(item => item.id == salary.id);
    this.employeeData.salaries[itemIndex] = salary;
  }

  public save(): void {
    console.warn(this.formGroup.value);
    let id: Guid;
    //Saving Employee Data
    this.rest.addUpdateEmployee(this.formGroup.value).subscribe((resp: apiResponse) => {
      console.log(resp);
      if (resp.isError != true) {
        //Saving Salaries
        id = resp.data.id;
        if (this.employeeData.salaries.length != 0) {
          for (let sal of this.employeeData.salaries) {
            sal.employeeId = id;
            if (sal.modified == true) {
              this.saveDetail(sal);
            }
          }
        }
        this.hasChange = false;
        this.hasSalariesChange = false;
        const navigationEditForm: string[] = ['/employee-edit/'];
        navigationEditForm.push(id.toString());
        this.router.navigate(navigationEditForm);
        this.matSnackBar.open('Saved Correctly', 'Okay', { duration: 3000 });
      }
      else {
        this.matSnackBar.open(resp.message,'Close',{duration : 3000});
      }

      //this.formGroup.get('officeName').setValue(this.employeeData.officeId);
    }
    );
  }

  saveDetail(salary: SalaryReponse) {
    this.rest.addUpdateEmployeeSalary(salary).subscribe((resp: apiResponse) => {
      console.log(resp);
      if (resp.isError == true) {
        this.matSnackBar.open(resp.message, 'Close',{duration: 3000});
      }
    });
  }

}
